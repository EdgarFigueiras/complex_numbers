#This program tests the functionalities of the class_complex class
from class_complex import ComplexNumber
import matplotlib.pyplot as plt

cn1=ComplexNumber()
cn2=ComplexNumber(1.0, -4.0)
cn3=ComplexNumber(3.0, 2.0)
cn4=ComplexNumber(2.0, -3.0)

print("Variables")
print("cn1 = ", cn1.to_string())
print("cn2 = ", cn2.to_string())
print("cn3 = ", cn3.to_string())
print("cn4 = ", cn4.to_string())

print("Operations")

cn_sum=cn2+cn3
print("cn2 + cn3= ", cn_sum.to_string())

cn_mul=cn3+cn4
print("cn3 * cn4= ", cn_mul.to_string())

cn2.conjugate()
print("cn2 conjugate= ", cn2.to_string())