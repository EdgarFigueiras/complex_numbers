class ComplexNumber(object):
	real=0.0
	imag=0.0
	
	#Constructor, by default sets real and imaginary parts to 0.0
	def __init__(self, r=0.0, i=0.0):
		self.real=r
		self.imag=i
	
	#Conjugates de complex part
	def conjugate(self):
		self.imag=-self.imag

	#Operator + overloaded
	def __add__(self, c):
		complex_sum	= ComplexNumber()
		complex_sum.real = self.real + c.real
		complex_sum.imag = self.imag + c.imag
		return complex_sum

	__radd__ = __add__

	#Operator * overloaded
	def __mul__(self, c):
		complex_mul	= ComplexNumber()
		complex_mul.real = (self.real*c.real)-(self.imag*c.imag) 
		complex_mul.imag = (self.real*c.imag)+(self.imag*c.real) 
		return complex_mul

	__rmul__ = __mul__

	#Generates a readable string
	def to_string(self):
		text_return = ""
		if self.imag>=0:
			text_return = str(self.real)+" + i"+str(abs(self.imag))
		else:
			text_return = str(self.real)+" - i"+str(abs(self.imag))
		return text_return

