## Complex numbers class in Python3.##
Adds funtionalities like conjugate, overloads the + and the * operators and formats the string output to make the results more readable.

Functions implemented:

1. Conjugate 

2. "+" operator overload 

3. "*" operator overload

4. to_string function

Example of the output of the test file:
![Captura de pantalla 2017-07-13 a las 11.35.55.png](https://bitbucket.org/repo/p49M7KE/images/692205044-Captura%20de%20pantalla%202017-07-13%20a%20las%2011.35.55.png)